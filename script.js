let buttons = document.querySelectorAll('button')
let screen = document.querySelector('.screen')


for(let i = 0; i < buttons.length; ++i) {
    buttons[i].onclick = function() {
        try {
            let s = screen.innerText;
            if (this.innerText == '=') {
                screen.innerText = eval(s)
            } else if ( this.innerText == 'AC') {
                screen.innerText = ''
            } else if( this.innerText == '<-') {
                screen.innerText = s.slice(0, s.length - 1)
            } else {
                screen.innerText = s + this.innerText
            }
        } catch(e) {
            alert("sitax error")
            screen.innerText = ''
        }
    }
}
